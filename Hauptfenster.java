import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 13.04.2021
 * @author 
 */

public class Hauptfenster extends JFrame {
  // Anfang Attribute
  private JButton jButton1 = new JButton();
  // Ende Attribute
  
  public Hauptfenster() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300;
    int frameHeight = 300;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Hauptfenster");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten    
    jButton1.setBounds(48, 48, 185, 65);
    jButton1.setText("jButton1");
    jButton1.setMargin(new Insets(2, 2, 2, 2));
    jButton1.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jButton1_ActionPerformed(evt);
      }
    });
    cp.add(jButton1);
    // Ende Komponenten    
    setVisible(true);
  } // end of public Hauptfenster
  
  // Anfang Methoden
  public void jButton1_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    MeineWelt welt1 = new MeineWelt();
    welt1.ZiegelVerstreuen(10,2);
    MeinRoboter robi1 = new MeinRoboter(welt1);
  } // end of jButton1_ActionPerformed

  // Ende Methoden
} // end of class Hauptfenster

